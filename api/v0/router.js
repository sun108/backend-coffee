
var router = async function(method,conllection,value,key) {
    var model = require('./service');
    switch(method){
      case 'GET':
          var ret = await model.queryMany_service(conllection,value,key)
          return ret
        break;
      case 'POST':
          var ret = await model.InsertMany_service(conllection,value,key)
          return ret
        break;
      case 'DELETE':
          var ret = await model.deleteMany_service(conllection,value,key)
          return ret
        break;
      case 'PUT':
          var find = value[0];
          var valUpdate = value[1];
          var ret = await model.updateOne_service(conllection,find,valUpdate)
          return ret
        break;
    }
  };
  
  module.exports = router;