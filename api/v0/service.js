const { func_addIdCode_arrBoj,func_random_string } = require(global.appRoot + "/res/func_general");
const db_driver = require(global.appRoot + "/driver/"+ process.env.WORK_DB_DRIVER+"/"+process.env.WORK_DB_FILE);
var result = null

module.exports = {
  set_database: async (data) => {
    db.met_mongodb_setCollection(data)
  },
  queryOne_service:async (db,data) => {
    await module.exports.set_database(db)
    return await db.met_mongodb_query(data)
  },
  queryMany_service:async (dbn,arrIdCode,key, callBack) => {
    try{
        var db = new db_driver();
        db.met_mongodb_setDatabase(process.env.MONGO_WORK_DB);
        result = await db.met_mongodb_queryInArray(dbn, arrIdCode,key )
    }catch (err) {
        result = err
    }
    return result
  },
  queryCheckItemProduct_service:async (dbn,arrIdCode,key, callBack) => {
    try{
        var db = new db_driver();
        db.met_mongodb_setDatabase(process.env.MONGO_WORK_DB);
        result = await db.met_mongodb_queryInArray(dbn, arrIdCode,key )
    }catch (err) {
        result = err
    }
    return result
  },
  deleteMany_service:async (dbn,arrIdCode,key, callBack) => {
    try{
      var db = new db_driver();
      db.met_mongodb_setDatabase(process.env.MONGO_WORK_DB);
      console.log(key)
      result = await db.met_mongodb_deleteInArrayObject( dbn,arrIdCode,key )
    }catch (err) {
      result = err
    }
    return result
  },
  updateOne_service:async (dbn,find,valUpdate, callBack) => {
    console.log(dbn,find,valUpdate)
    try{
      var db = new db_driver();
      db.met_mongodb_setDatabase(process.env.MONGO_WORK_DB);
      result = await db.met_mongodb_updateOne(dbn,find,valUpdate)
    }catch (err) {
      result = err
    }
    return result
  },
  InsertMany_service:async (dbn,dataObj,key, callBack) => {
    for(var i = 0; i < dataObj.length ; i++){
      var idCode = func_random_string(process.env.IDCODE_COUNT)
      dataObj[i][`${key}_idcode`] = `${key}idcode_${idCode}`;
    }
    try{
      console.log(dataObj)
      var db = new db_driver();
      db.met_mongodb_setDatabase(process.env.MONGO_WORK_DB);
      result = await db.met_mongodb_insertInArrayObject(dbn, dataObj )
    }catch (err) {
      result = err
    }
    return result
  },
  queryOrder_onDate:async (date, callBack) => {
    try{
        var db = new db_driver();
        db.met_mongodb_setDatabase(process.env.MONGO_WORK_DB);
        result = await db.met_mongodb_queryOrderOnDate(date )
    }catch (err) {
        result = err
    }
    return result
  }
};