global.std = require('./output/standard');
const {getdatetime , getdateonly} = require('./services/datetime/datetime')
const fs = require("fs");
var _ = require('lodash');
var hasService = true;var conn_db = true;
var wording_error_cannotfindversionAPI=`can't find your version API`
var wording_error_cannotfindyourqueryselect=`can't find your query select`
var wording_error_cannotsaleissomethingwrong=`can't sale had something wrong item`
var wording_error_cannotfindyouritem=`can't find your idcode item`
var wording_error_failconnecttodatabase=`can't connect to database`
var wording_error_parameterwrong=`your parameter wrong`

//============== function query ======================================
async function set_routAPIQuery(req){
  var data=[],objData = null,version = [];
  var path_uri="./api/",keyc = null,sucs=false,mgs=null
  version = checkPathVersion(req.params.version)
  if(version !== []){
    var val = Object.values(req.query);
    var key = Object.keys(req.query);
    path_uri = `./api/${version}/${process.env.ROUTER}`
    const router_api = require(path_uri);
    switch(req.params.selectItem){
      case "coffee":
        var res = await router_api(req.method,'product_coffee',['all'],keyc)
        data.push(res)
        sucs=true
      break;
      case "state":
        var res = await router_api(req.method,'product_state',['all'],keyc)
        data.push(res)
        sucs=true
      break;
      case "option":
        var res = await router_api(req.method,'product_option',['all'],keyc)
        data.push(res)
        sucs=true
      break;
      case "menucoffee":
        var res_coffee = await router_api(req.method,'product_coffee',['all'],keyc)
        var res_state = await router_api(req.method,'product_state',['all'],keyc)
        var res_option = await router_api(req.method,'product_option',['all'],keyc)
        data = [
          {'coffee':res_coffee},
          {'state':res_state},
          {'option':res_option}
        ]
        sucs=true
      break;
      case "order":
        var dataObjArr = []
        if(key[0] === `date`){
          path_uri = `./api/${version}/${process.env.SERVICE}`
          const service_api = require(path_uri);
          sucs=true
          if(val[0] === `today`){
            var date = getdateonly()
            dataObjArr = await service_api.queryOrder_onDate(date)
          }else{
            dataObjArr = await service_api.queryOrder_onDate(val[0])
          }
          data = dataObjArr
        }
      break;
      case "summary"://difficult
        var dataObjArr = [], total_summary_price=0, total_order_amount=0
        if(key[0] === `date`){
          path_uri = `./api/${version}/${process.env.SERVICE}`
          const service_api = require(path_uri);
          sucs=true
          if(val[0] === `today`){
            var date = getdateonly()
            dataObjArr = await service_api.queryOrder_onDate(date)
          }else{
            dataObjArr = await service_api.queryOrder_onDate(val[0])
          }
          dataObjArr.forEach(function(x) {
            total_summary_price += x.order_summary_price
            total_order_amount += x.order_amount
          })
          data.push({
            "total_order_count":dataObjArr.length,
            "total_order_amount":total_order_amount,
            "total_summary_price":total_summary_price,
            "total_list":dataObjArr
          })
        }
      break;
      default:
        mgs=wording_error_cannotfindyourqueryselect
    }
  }else{
    mgs=wording_error_cannotfindversionAPI
  }
  return func_response(sucs,mgs,data);
}

//============== function get item from sale ======================================
async function set_routAPIGetItemFromSale(req){
  var data=[],objData = null,version = [],arrSelectItem=[],getItemForQueryAll=[]
  var path_uri="./api/",keyc = null,sucs=false,mgs=null
  version = checkPathVersion(req.params.version)
  if(version !== []){
    var val = Object.values(req.query);
    var key = Object.keys(req.query);
    path_uri = `./api/${version}/${process.env.ROUTER}`
    const router_api = require(path_uri);
    if(key.length >= 2 && key[0] === `sort_amount` && key[1] === `date`){
      if(val[0] === `desc` || val[0] === `asc`){
        var dataObjArr = []
        path_uri = `./api/${version}/${process.env.SERVICE}`
        const service_api = require(path_uri);
        if(val[1] === `today`){
          var date = getdateonly()
          dataObjArr = await service_api.queryOrder_onDate(date)
        }else{
          dataObjArr = await service_api.queryOrder_onDate(val[1])
        }
        switch(req.params.selectItem){
          case "coffee":
            getItemForQueryAll = await router_api(req.method,'product_coffee',['all'],keyc)
            getItemForQueryAll.forEach(function(x) {
              arrSelectItem.push({"idcode":x.coffee_idcode,"amount":0,"total_price":0,"name":x.coffee_name})
            });
            dataObjArr.forEach(function(x) {
              x.order_list.forEach(function(d){
                var findIndex = arrSelectItem.findIndex(t=>t[`idcode`] === d.coffee[0].idcode)
                if(findIndex > -1){
                  arrSelectItem[findIndex].amount += d.amount
                  arrSelectItem[findIndex].total_price += d.price
                }
              })
            });
            arrSelectItem = _.orderBy(arrSelectItem,['amount'],[val[0]])
            data=arrSelectItem
            sucs=true
            break;
          case "option":
            getItemForQueryAll = await router_api(req.method,'product_option',['all'],keyc)
            getItemForQueryAll.forEach(function(x) {
              arrSelectItem.push({"idcode":x.option_idcode,"amount":0,"total_price":0,"name":x.option_name,"add_price":x.add_price})
            });
            dataObjArr.forEach(function(g) {
              g.order_list.forEach(function(s){
                if(s.option[0].length > 0){
                  s.option[0].forEach(function(q){
                    var findIndex = arrSelectItem.findIndex(t=>t[`idcode`] === q.idcode)
                    if(findIndex > -1){
                      arrSelectItem[findIndex].amount += q.amount
                      arrSelectItem[findIndex].total_price += q.price
                    }
                  });
                }
              });
            });
            arrSelectItem = _.orderBy(arrSelectItem,['amount'],[val[0]])
            data=arrSelectItem
            sucs=true
          break;
        }
      }else{
        mgs = wording_error_parameterwrong
      }
    }else{
      mgs = wording_error_parameterwrong
    }
  }else{
    mgs=wording_error_cannotfindversionAPI
  }
  return func_response(sucs,mgs,data);
}
//============== function action item product ======================================
async function set_routAPIAction(req , datax, medthod , collection){
  var data = [], objData = null, path_uri="./api/",sucs=false,mgs=null
  var result_query=null, keyc=null ,arrCheck=[], isIsset=false
  version = checkPathVersion(req.params.version)
  if(version !== []){
    switch(medthod){
      case 'POST':
        objData = datax
        keyc=`${collection}`
        break;
      case 'PUT':
        objData = [ datax[0], {$set: datax[1]} ]
        arrCheck.push(datax[0][`${collection}_idcode`])
        break;
      case 'DELETE':
        objData = datax
        arrCheck=datax
        keyc=`${collection}_idcode`
        break;
    }
    if(medthod === `PUT` || medthod === `DELETE`){
      var checkRes = await func_checkItemProduct(version,`product_${collection}`,arrCheck,`${collection}_idcode`)
      if(checkRes){
        isIsset = true
      }
    }else{
      isIsset = true
    }
    if(isIsset){
      const router_api = require(`./api/${version}/${process.env.ROUTER}`);
      result_query = await router_api(medthod,`product_${collection}`,objData,keyc)
      if(result_query == process.env.ERROR_CONN_DB){
        conn_db = false;
        mgs = wording_error_failconnecttodatabase
      }else{
        if(medthod === `POST`){
          data.push(result_query.ops[0])
          sucs=true;
        }else{
          sucs=true;
        }
      }
    }else{
      mgs = wording_error_cannotfindyouritem
    }
  }else{
    mgs=wording_error_cannotfindversionAPI
  }
  return func_response(sucs,mgs,data);
}

//============= function sale product =================================================
async function set_routeSaleProduct(req){
  var data=[],objData = null,version = [];
  var path_uri="./api/",keyc = null,sucs=false,mgs=null
  version = checkPathVersion(req.params.version)
  if(version !== []){
    path_uri = `./api/${version}/${process.env.ROUTER}`
    var order_ondatetime = getdatetime(), order_amount=0, order_summary_price=0, order_list=[]
    // order_getcash = req.body.getcash, hasMoneyEnough = false ,payment_idocde = req.body.payment_idocde ,order_change=0

    var coffeeArr=[],stateArr=[],optionArr=[],optionInArr=[]
    //loop for get idcode push to array from orderList
    req.body.order_list.forEach(function(x) {
      coffeeArr.push(x.coffee_idcode)
      stateArr.push(x.state_idcode)
      optionArr.push(x.option)
      order_amount+=x.amount
    });
    //loop for get idcode all option push to array
    for(i=0 ; i < optionArr.length ; i++){
      optionArr[i].forEach(function(y){
        optionInArr.push(y.option_idcode)
      })
    }
    //check coffee / state / option / checkPayment (hard code for finish on time)
    var getCoffee = await func_getItemProduct(version,`product_coffee`,coffeeArr,`coffee_idcode`)
    var getState = await func_getItemProduct(version,`product_state`,stateArr,`state_idcode`)
    var getOption = await func_getItemProduct(version,`product_option`,optionInArr,`option_idcode`)
    // var getPayment = await func_getItemProduct(version,`payment_method`,[payment_idocde],`payment_idcode`)
    if(
      getCoffee.length == coffeeArr.length && getState.length == stateArr.length && 
      getOption.length == optionInArr.length 
      // && getPayment.length>0
    ){
      req.body.order_list.forEach(function(x) {
        var findCoffee = getCoffee.find(v=>v[`coffee_idcode`] === x.coffee_idcode)
        var findState = getState.find(v=>v[`state_idcode`] === x.state_idcode)
        var optionObjArr = setObjArr_for_option(x.option,getOption)
        var thisListPrice = (optionObjArr[2] + findCoffee.price + findState.add_price) * x.amount
        order_summary_price += thisListPrice

        order_list.push({
          "coffee":[{"idcode":findCoffee.coffee_idcode},{"name":findCoffee.coffee_name},{"price":findCoffee.price}],
          "state":[{"idcode":findState.state_idcode},{"name":findState.state_name},{"add_price":findState.add_price}],
          "option":[ optionObjArr[0] ],
          "option_amount":optionObjArr[1],
          "option_price":optionObjArr[2],
          "amount":x.amount,
          "price": thisListPrice
        })
      });
      data.push({
        "order_list":order_list,
        "order_summary_price":order_summary_price,
        // "order_getcash":order_getcash,
        // "order_change":order_change,
        "order_amount":order_amount,
        // "payment_idcode":payment_idocde,
        "order_ondatetime":order_ondatetime
      })
      sucs = true
      const router_api = require(path_uri);
      result_query = await router_api(`POST`,`order_query`,data,`order`)
      // if(getPayment[0].change){
      //   if(order_getcash => order_summary_price){
      //     hasMoneyEnough = true
      //   }
      // }else{
      //   hasMoneyEnough = true
      // }
      // if(hasMoneyEnough){
        
      // }
    }else{
      mgs = wording_error_cannotsaleissomethingwrong
    }
  }else{
    mgs = wording_error_cannotfindversionAPI
  }
  return func_response(sucs,mgs,data);
}
function setObjArr_for_option(optionList,getOption){
  var res = [], option_amount=0 ,option_price =0, opPrice=0
  optionList.forEach(function(a){
    var findInOption = getOption.find(v=>v[`option_idcode`] === a.option_idcode)
    opPrice=findInOption.add_price * a.option_amoun
    res.push({
      "idcode":findInOption.option_idcode,
      "name":findInOption.option_name,
      "add_price":findInOption.add_price,
      "amount":a.option_amoun,
      "price":opPrice
    })
    option_amount += a.option_amoun
    option_price += opPrice
  })
  return [res,option_amount,option_price];
}

//============= function check and get item =================================================
async function func_checkItemProduct(version,dbn,value,key){
  var res = false
  const service_api = require(`./api/${version}/${process.env.SERVICE}`);
  var resCheckItem = await service_api.queryCheckItemProduct_service(dbn,value,key)
  if(value.length === resCheckItem.length){
    res = true;
  }
  return res
}
async function func_getItemProduct(version,dbn,value,key){
  var res = false
  const service_api = require(`./api/${version}/${process.env.SERVICE}`);
  var resGetItem = await service_api.queryCheckItemProduct_service(dbn,value,key)
  return resGetItem
}

//============= function response =================================================
async function func_response(sucs,mgs,data){
  if(!hasService){
    data = null;
    return global.std(false,null,404,data)
  }if(!conn_db){
    data = null;
    var res = await global.std(false,process.env.ERROR_CONN_DB,500,data)
    return res
  }else{
    var res = await global.std(sucs,mgs,200,data)
    // console.log(res)
    return res
  }
}




//============= function check and other function =================================================
//==============================================================
//==============================================================
//==============================================================
function check_version(path){
  if (fs.existsSync(path)) {
      return true;
  }
  return false;
}

function get_versionLatest(path){
  var x = fs.readdirSync(path).filter(function (file) {
      return fs.statSync(path+file).isDirectory();
  });
  x = get_CutVersionString(x);
  x = get_filterOnlyNumberInArray(x)
  return Math.max.apply(Math, x )
}

function check_fileController(path){
  try {
      if (fs.existsSync(path)) {
        return true
      }
  }catch(err) {
      return false
  }
}

function get_CutVersionString(arr){
  return arr.map(i => i.replace(process.env.VERSION,''));
}

function get_filterOnlyNumberInArray(arr){
  var n_arr = []
  arr.filter(function (item) {
      if( (parseInt(item) == item) ){
          n_arr.push(parseInt(item));
      }
  });
  return n_arr
}

function checkPathVersion(paramVersion){
  var path_uri="./api/",version = []
  if(paramVersion === process.env.LAST){
    version = process.env.VERSION + get_versionLatest(path_uri)
  }else if(check_version(`./api/${paramVersion}`)){
    version = paramVersion
  }
  return version
}

module.exports = { 
  set_routAPIQuery , 
  func_response , 
  set_routAPIAction ,
  set_routeSaleProduct ,
  set_routAPIGetItemFromSale
}