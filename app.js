const path_env = "./config/.env"
require("dotenv").config({path:path_env});
require('console-stamp')(console, { pattern: 'dd/mm/yyyy HH:MM:ss.l' });
global.std = require('./output/standard');
const {
  set_routAPIQuery , 
  func_response ,
  set_routAPIGetItemFromSale,
  set_routAPIAction ,
  set_routeSaleProduct
} = require('./module.js')
const express = require("express");
const fs = require("fs");
const cors = require('cors')
const app = express({caseSensitive: true});
const timeout = require('connect-timeout')
const multer = require('multer');

var cookieParser = require('cookie-parser');
var session = require('express-session');
const { json } = require("express");

const router = express.Router();
router.use(cors())

config_dir()
app.use(timeout(process.env.TIMEOUT))
app.use(cookieParser());
app.use(haltOnTimedout)
app.use(express.json());

// ================= ROUT ============================

router.get('/api/:version/query/:selectItem', async function(req, res, next){
  res.json( await set_routAPIQuery(req) )
})

router.get('/api/:version/getitemsale/:selectItem', async function(req, res, next){
  res.json( await set_routAPIGetItemFromSale(req) )
})

router.route('/api/:version/action/:selectItem')
  .post(async function(req, res, next){
    res.json( await set_routAPIAction(req, req.body , req.method, req.params.selectItem) )
  })
  .put(async function(req, res, next){
    res.json( await set_routAPIAction(req, req.body , req.method, req.params.selectItem) )
  })
  .delete(async function(req, res, next){
    res.json( await set_routAPIAction(req, req.body , req.method, req.params.selectItem) )
  })

  router.post('/api/:version/sale/product', async function(req, res, next){
    res.json( await set_routeSaleProduct(req) )
  })

// ================= ROUT ============================

router.use(function(req, res){
  res.json( global.std(false,'route not found',404,null) )
});
app.use(router);
app.listen(process.env.PORT_API_WORK, (req, res) => {
  console.log("server up and running on PORT :", process.env.PORT_API_WORK);
});

function haltOnTimedout (req, res, next) {
  if (!req.timedout) {
    next()
  }else{
    console.log('========================== time out ===================================')
    res.json( global.std(false,'error',500,null) )
  }
}

function config_dir(){
  var path = require('path');
  global.appRoot = path.resolve(__dirname)+'/';
}

