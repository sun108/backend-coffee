function output(success ,message ,error_code ,data){
    if(message === null || message == ''){
        message = get_message_standard(error_code)
    }
    return { 
        success: success, 
        message: message,
        connect_code: error_code, 
        data: data
    }
}

function get_message_standard(code){
    switch(code){
        case 200:
            return 'connected!'
        break;
        case 401:
            return 'Unauthorized!'
        break;
        case 403:
            return 'Forbidden!'
        break;
        case 404:
            return 'Not Found!'
        break;
        case 405:
            return 'Method Not Allowed!'
        break;
        case 408:
            return 'Request Timeout!'
        break;
        case 413:
            return 'Request Entity Too Large!'
        break;
        case 423:
            return 'Locked!'
        break;

        case 500:
            return 'Internal Server Error!'
        break;
        case 501:
            return 'Not Implemented!'
        break;
        case 502:
            return 'Bad Gateway!'
        break;
        case 503:
            return 'Service Unavailable!'
        break;

        case x:
    }
}

module.exports = output;