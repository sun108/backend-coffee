const moment = require('moment');

getdatetime =()=>{
    var res = moment(new Date()).add(7, 'hours').format("YYYY-MM-DD HH:mm:ss")
    return res
}

getdateonly =()=>{
    // hard code for finish work on time!!
    // var res = moment(new Date()).add(7, 'hours').format("YYYY-MM-DD HH:mm:ss")
    var res = moment(new Date()).add(7, 'hours').format("YYYY-MM-DD")
    return res
}

module.exports = {
    getdatetime , getdateonly
};