module.exports = {
    func_random_string: (length) => {
        var result = '';
        var characters = 'ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789';
        var charactersLength = characters.length;
        for ( var i = 0; i < length; i++ ) {
            result += characters.charAt(Math.floor(Math.random() * charactersLength));
        }
        return result;
    },
    func_addIdCode_arrBoj: (obj,idcode) =>{
        obj.idcode = idcode
        var jsonIssues = [];
        jsonIssues.push( obj );
        return jsonIssues;
    }
}