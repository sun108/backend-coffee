var MongoClient = require('mongodb').MongoClient
const url = 'mongodb://'+ process.env.MONGO_WORK_USER + ':' + process.env.MONGO_WORK_PASSWORD + '@' + process.env.MONGO_HOST +':' + process.env.MONGO_WORK_PORT + '/';
const config = {
  serverSelectionTimeoutMS: process.env.MONGO_CON_TIMEOUT,
  connectTimeoutMS: process.env.MONGO_CON_TIMEOUT,
  socketTimeoutMS: process.env.MONGO_CON_TIMEOUT,
  useUnifiedTopology: true,
  useNewUrlParser: true
}
class mongodb {
  constructor(){
    this.met_mongodb_connect()
  }

  met_mongodb_setDatabase(db_name){
    this.database = db_name
  }

  met_mongodb_setCollection(collect_name){
    this.collection = collect_name
  }

  met_mongodb_connect(){
    this.client = new MongoClient(url, config);
  }

  met_mongodb_isConnected() {
    return !!this.client
  }

  met_mongodb_reCheckConnect(){
    if(!this.met_mongodb_isConnected()){
      this.met_mongodb_connect()
    }else{
    }
  }

  met_mongodb_closedb(){
    if(this.met_mongodb_isConnected){
      try{
        this.client.close();
      }catch(err){
        
      }
    }
  }

  met_mongodb_CreateNewConnection(dbn){
    this.met_mongodb_connect()
    this.met_mongodb_setCollection(dbn)
    this.met_mongodb_reCheckConnect()
  }

  met_mongodb_query(query){
    this.met_mongodb_reCheckConnect()
    return new Promise((resolve, reject) => {
      this.client.connect()
      .then((client)=>{
        var db = client.db(this.database)
        var col = db.collection(this.collection)
        col.find(query).toArray(function (err, result) {
          if (!err) {
            resolve(result)
            client.close();
          } else {
            resolve(err)
            client.close();
          }
        })
      })
      .catch(function (err) {
        client.close();
        reject(process.env.ERROR_CONN_DB)
      })
      .finally(()=>{ this.met_mongodb_closedb() })
    })
  }

  met_mongodb_query_all_with_datetime(dbn,query,key,time){
    // console.log('5 >>>>>>>>> enter connect = ' + query)
    this.met_mongodb_CreateNewConnection(dbn)
    return new Promise ( (resolve, reject) => {
      this.client.connect()
      .then((client)=>{
        var db = client.db(this.database)
        var col = db.collection(this.collection)
        if(query == 'all'){
          // console.log('6 >>>>>>>>> enter connected = ' + query)
          col.find(time[0]).toArray(function (err, result) {
            if (!err) {
              // console.log('7.1 >>>>>>>>> connected not error | result = ' + result)
              resolve(result)
              client.close();
            } else {
              // console.log('7.2 >>>>>>>>> connected error = ' + err)
              resolve(err)
              client.close();
            }
          })
        }
      })
      .catch(function (err) {
        // console.log('7.3 >>>>>>>>> catch connected error = ' + query)
        // console.log(this.collection)
        client.close();
        reject(process.env.ERROR_CONN_DB)
      })
      .finally(()=>{  
        // console.log('7.4 >>>>>>>>> finally connected error = ' + query) ;
        // this.met_mongodb_closedb() 
      })
    })
  }

  met_mongodb_queryInArray(dbn,query,key){
    this.met_mongodb_CreateNewConnection(dbn)
    return new Promise ( (resolve, reject) => {
      this.client.connect()
      .then((client)=>{
        var db = client.db(this.database)
        var col = db.collection(this.collection)
        if(query[0] == 'all'){
          col.find().toArray(function (err, result) {
            if (!err) {
              resolve(result)
              client.close();
            } else {
              resolve(err)
              client.close();
            }
          })
        }else{
          var setKey = 'idcode'
          if(key !== null){
            setKey = key
          }
          col.find({[setKey]:{ $in:query}}).toArray(function (err, result) {
            if (!err) {
              resolve(result)
              client.close();
            } else {
              resolve(err)
              client.close();
            }
          })
        }
      })
      .catch(function (err) {
        client.close();
        reject(process.env.ERROR_CONN_DB)
      })
      .finally(()=>{  
      })
    })
  }

  met_mongodb_queryOrderOnDate(date){
    this.met_mongodb_CreateNewConnection(`order_query`)
    return new Promise ( (resolve, reject) => {
      this.client.connect()
      .then((client)=>{
        var db = client.db(this.database)
        var col = db.collection(this.collection)
        col.find({
          "order_ondatetime":
          {$gte: date+' 00:00:00',$lt: date+" 23:59:59"}
        }).toArray(function (err, result) {
          if (!err) {
            resolve(result)
            client.close();
          } else {
            resolve(err)
            client.close();
          }
        })
      })
      .catch(function (err) {
        client.close();
        reject(process.env.ERROR_CONN_DB)
      })
      .finally(()=>{})
    })
  }

  met_mongodb_insertInArrayObject(dbn,myobj){
    this.met_mongodb_CreateNewConnection(dbn)
    return new Promise((resolve, reject) => {
      this.client.connect()
      .then((client)=>{
        var db = client.db(this.database)
        var col = db.collection(this.collection)
        col.insertMany(myobj, function(err, result) {
          if (!err) {
            resolve(result)
            client.close();
          } else {
            resolve(err)
            client.close();
          }
        })
      })
      .catch(function (err) {
        client.close();
        reject(process.env.ERROR_CONN_DB)
      })
      .finally(()=>{})
    })
  }

  met_mongodb_deleteInArrayObject(dbn,myobj,key){
    this.met_mongodb_CreateNewConnection(dbn)
    return new Promise((resolve, reject) => {
      this.client.connect()
      .then((client)=>{
        var db = client.db(this.database)
        var col = db.collection(this.collection)
        col.deleteMany({[key]:{ $in:myobj}}, function(err, result) {
          if (!err) {
            resolve(result)
            client.close();
          } else {
            console.log(err)
            resolve(err)
            client.close();
          }
        })
      })
      .catch(function (err) {
        console.log(`catch ${err}`)
        client.close();
        reject(process.env.ERROR_CONN_DB)
      })
      .finally(()=>{ })
    })
  }

  met_mongodb_updateOne(dbn,find,valUpdate){
    this.met_mongodb_CreateNewConnection(dbn)
    return new Promise((resolve, reject) => {
      this.client.connect()
      .then((client)=>{
        var db = client.db(this.database)
        var col = db.collection(this.collection)
        col.updateOne(find,valUpdate,{ upsert: true }, function(err, result) {
          if (!err) {
            resolve(true)
            client.close();
          } else {
            resolve(false)
            client.close();
          }
        })
      })
      .catch(function (err) {
        client.close();
        reject(process.env.ERROR_CONN_DB)
      })
      .finally(()=>{})
    })
  }

  met_mongodb_queryLeaveworkRound(dataStart,dataEnd){
    // console.log(dataStart,dataEnd)
    this.met_mongodb_CreateNewConnection("data_leaveWork")
    return new Promise ( (resolve, reject) => {
      this.client.connect()
      .then((client)=>{
        var db = client.db(this.database)
        var col = db.collection(this.collection)
          col.find(
            {$and: [
              {"dateStart":
                {$gte:`${dataStart} 00:00:00`,$lt:`${dataEnd} 00:00:00`}
              }
            ]}
          ).sort({"dateCreate":-1})
          .toArray(function (err, result) {
            if (!err) {
              resolve(result)
              client.close();
            } else {
              // console.log(`err ${err}`)
              resolve(err)
              client.close();
            }
          })
      })
      .catch(function (err) {
        client.close();
        reject(process.env.ERROR_CONN_DB)
      })
      .finally(()=>{
        // this.met_mongodb_closedb() 
      })
    })
  }

  met_mongodb_updateAppConfig(dataArrayObj){
    this.met_mongodb_CreateNewConnection("app_config")
    return new Promise ( (resolve, reject) => {
      this.client.connect()
      .then((client)=>{
        var db = client.db(this.database)
        var col = db.collection(this.collection)
        col.bulkWrite(dataArrayObj, (err,result) => {
            if (!err) {
              resolve(result)
              client.close();
            } else {
              // console.log(`err ${err}`)
              reject(err)
              client.close();
            }
          })
      })
      .catch(function (err) {
        // console.log(`try err ${err}`)
        client.close();
        reject(process.env.ERROR_CONN_DB)
      })
      .finally(()=>{
        // this.met_mongodb_closedb() 
      })
    })
  }

  met_mongodb_clearCollectionAllDataField(dnn){
    this.met_mongodb_CreateNewConnection(dnn)
    return new Promise ( (resolve, reject) => {
      this.client.connect()
      .then((client)=>{
        var db = client.db(this.database)
        var col = db.collection(this.collection)
        col.remove({}, function(err, result) {
          if (!err) {
            // resolve(result)
            resolve(true)
            client.close();
          } else {
            resolve(false)
            // resolve(err)
            client.close();
          }
        })
      })
      .catch(function (err) {
        client.close();
        reject(process.env.ERROR_CONN_DB)
      })
      .finally(()=>{
        // this.met_mongodb_closedb() 
      })
    })
  }
}

module.exports = mongodb